import { RepositoryItem } from "./RepositoryItem";
import  '../styles/Repositories.scss'
import { useEffect, useState } from 'react';

export function RepositoryList() {
    const [repositories, setRepositories] = useState([])

    useEffect(() => {
        fetch("https://api.github.com/users/kasumo-spec/repos")
        .then(response => response.json())
        .then(data => setRepositories(data))
        .catch(err => console.log(err))
    }, [])

    return (
        <section className="repository-list">
            <h1>Lista de Repositórios</h1>

            <ul>
                {repositories.map((repository) => {
                    return <RepositoryItem 
                            key={repository.name} 
                            repository={repository} />
                })}
            </ul>
        </section>
    )
}